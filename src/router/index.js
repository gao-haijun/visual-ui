import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Dashboard', icon: 'dashboard' }
    }]
  },

  //疫情地图
  {
    path: '/map',
    component: Layout,
    children: [
      {
        path: "index",
        component: () => import('@/pages/map'),
        meta: { title: '疫情地图', icon: 'map' }
      },
     {
      path:"maps",
      component: () => import('@/pages/maps'),
      hidden:true,
     }
    ]
  },

  //国内实时疫情网页
  {
    path: '/guonei_realTime',
    component: Layout,
    children: [
      {
        path: "index",
        component: () => import('@/pages/guonei_realTime'),
        meta: { title: '国内实时疫情', icon: 'China' }
      }
    ]
  },
  //国外实时疫情网页
  {
    path: '/guowai_realTime',
    component: Layout,
    children: [
      {
        path: "index",
        component: () => import('@/pages/guowai_realTime'),
        meta: { title: '国外实时疫情', icon: 'earth' }
      }
    ]
  },
//疫情动态
{
  path: '/dynamic',
  component: Layout,
  children: [
    {
      path: "index",
      component: () => import('@/pages/dynamic'),
      meta: { title: '疫情动态', icon: 'form' }
    }
  ]
},
//数据统计
{
  path: '/statistics',
  component: Layout,
  children: [
    {
      path: "index",
      component: () => import('@/pages/statistics'),
      meta: { title: '数据统计', icon: 'shujutongji' }
    }
  ]
},


//新闻资讯
{
  path: '/news',
  component: Layout,
  children: [
    {
      path: "index",
      component: () => import('@/pages/news'),
      meta: { title: '新闻资讯', icon: 'eye' }
    }
  ]
},
//防控指南
{
  path: '/prevention',
  component: Layout,
  children: [
    {
      path: "index",
      component: () => import('@/pages/prevention'),
      meta: { title: '防控指南', icon: 'eye' }
    }
  ]
},
//定点医院
{
  path: '/hospital',
  component: Layout,
  children: [
    {
      path: "index",
      component: () => import('@/pages/hospital'),
      meta: { title: '定点医院', icon: 'eye' }
    }
  ]
},

  {
    path: '/base',
    component: Layout,
    name: 'base',
    meta: { title: '基础配置', icon: 'example' },
    children: [
      { 
        path:"user",
        component: () => import('@/pages/base/user'),
        meta: { title: '用户管理'}
      },
      { 
        path:"role",
        component: () => import('@/pages/base/role'),
        meta: { title: '角色管理'}
      },
      { 
        path:"privilege",
        component: () => import('@/pages/base/privilege'),
        meta: { title: '权限设置'}
      }
    ]
  },



  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
